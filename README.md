This plugins allows collect Bitcoin tips from drupal blog and article readers
 * Author: IhqTzup / Viljami Räisä
 * Prasos Oy
 * www.prasos.fi
 * www.bittiraha.fi
 * 
 * Inspiration and ideas came from http://terk.co/wordpress-bitcoin-tips-plugin/
 * all credits goes to Terk! Thank you!

Install like any drupal module
Create blockchain account
set identifier, password and other settings at admin/settings/bitcoin_tips/settings

TODO:

Automatic payout
Specific tips static
Payout address management