<?php

//TODO Improve API 

function bitcoin_tips_get_new_address($label){
    $guid = variable_get('bitcoin_tips_blockchain_identifier');
    $main_password = variable_get('bitcoin_tips_blockchain_main_password', '');
    
    $api_call = 'https://blockchain.info/merchant/'.$guid.'/new_address?password='.$main_password.'&label='.$label;
    
    set_time_limit(10);
    $result = @file_get_contents($api_call);
    if (!strlen($result))
        return false;
    
    $data = @json_decode($result);
    
    if (!$data)
        return false;
    
    if (!$data->address)
        return false;
    
    return $data->address;
}

function bitcoin_tips_get_address_data($address){
        $api_call = 'http://blockchain.info/address/'.$address.'?format=json';
        
        set_time_limit(10);
        $result = @file_get_contents($api_call);
        if (!strlen($result))
           return "";
        return @json_decode($result);
}
?>
